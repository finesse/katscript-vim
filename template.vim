" Vim syntax file
" Language: KatScript
" Maintainer: Philip Jones
" Latest Revision: 7 May 2021

if exists("b:current_syntax")
	finish
endif

let b:current_syntax = "kat"

" General philosophy / structure:
"
" The only top-level matches are comments and the special "katLineStart"
" group, so that we can control matches depending on position in the line.
" Everything else is "contained".
"
" "keywords" are used as much as possible rather than matches, as they're much
" more performant and make the code simpler. The main exception is for
" "katKeyword", as some of the keywords are very short and match node /
" parameter names, so we have to do some extra checking that they don't get
" matched where they shouldn't
"
" The somewhat unusual layout of this file is so that the element etc.
" definitions can be appended to the end by a generator script.

" Highlight groups
hi def link katTodo Todo
hi def link katComment Comment
hi def link katBoolean Boolean
hi def link katNumber Number
hi def link katOperator Operator
hi def link katCommand Function
hi def link katFunction Function
hi def link katElement Type
hi def link katKeyword Keyword
hi def link katString String
hi def link katName Identifier


"─────────────┐
" Basic types │
"─────────────┘

" Comments
syn match katComment display /#.*$/ contains=katTodo
syn keyword katTodo contained TODO FIXME

" Strings
syn region katString contained start=+'+ skip=+\\'+ end=+'+
syn region katString contained start=+"+ skip=+\\"+ end=+"+

" Values
" Number breakdown:
" \w\@<!               :  Must not be preceded by a word character
" [+-]\?               :  Optional sign
" \(\d*\.\d\+\|\d\+\)  :  One of x.xxx, .xxx or xxx
" \(...\|...\|...\)\?  :  Optionally one of the following:
"     [pnumkMGT]            :  An SI suffix
"     [eE][+-]\?\d\+[jJ]\?  :  An exponent (possibly with a complex suffix)
"     [jJ]                  :  A complex suffix
" \w\@!                :  Must not be proceded by a word character
syn match katNumber contained display /\w\@<![+-]\?\(\d*\.\d\+\|\d\+\)\([pnumkMGT]\|[eE][+-]\?\d\+[jJ]\?\|[jJ]\)\?\w\@!/
syn keyword katNumber contained inf infj infJ
syn keyword katBoolean contained True true False false

" Operators
syn match katOperator contained display /[+*/&-]/


"────────────────────┐
" Main control types │
"────────────────────┘

" Only "katElement" or "katCommand" can come at the beginning of a normal line
syn match katLineStart /^\s*[A-Za-z_]\@=/ nextgroup=katElement,katCommand

" An element must be followed by a name, and a command must be followed by
" brackets (which may span multiple lines)
syn match katName contained display /[A-Za-z_]\w*/ nextgroup=katRestOfLine
syn region katCommandArgs contained start="(" skip=/(\_.\{-})/ end=")" contains=katRestOfLine keepend
syn region katBrackets contained start="(" end=")" contains=katRestOfLine keepend

" After matching either an element + name or a command, we grab the rest of
" the line and contain all the rest of the rules.
syn match katRestOfLine contained /.*$/ contains=ALLBUT,katLineStart,katElement,katCommand,katName,katTodo,katCommandArgs


"──────────────────────────────┐
" Auto-generated keyword lists │
"──────────────────────────────┘


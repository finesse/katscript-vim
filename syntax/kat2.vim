" Vim syntax file
" Language: KatScript2
" Maintainer: Philip Jones
" Latest Revision: 7 May 2021

if exists("b:current_syntax")
	finish
endif

" General philosophy / structure:
"
" The only top-level matches are comments and the special "kat2LineStart"
" group, so that we can control matches depending on position in the line.
" Everything else is "contained".
"
" "keywords" are used as much as possible rather than matches, as they're much
" more performant and make the code simpler. The main exception is for
" "kat2Keyword", as some of the keywords are very short and match node /
" parameter names, so we have to do some extra checking that they don't get
" matched where they shouldn't
"
" The somewhat unusual layout of this file is to match the structure of
" kat.vim

" Highlight groups
hi def link kat2Todo Todo
hi def link kat2FtBlock Todo
hi def link kat2Comment Comment
hi def link kat2Number Number
hi def link kat2Operator Operator
hi def link kat2Command Keyword
hi def link kat2Function Function
hi def link kat2Element Type
hi def link kat2Name Identifier
hi def link kat2Variable Identifier


"─────────────┐
" Basic types │
"─────────────┘

" Comments
syn match kat2Comment display /[#%].*$/ contains=kat2Todo,kat2FtBlock
syn keyword kat2Todo contained TODO FIXME
" syn keyword kat2FtBlock contained FTblock FTend nextgroup=kat2Name skipwhite

" Operators
syn match kat2Operator contained display /[+*/-]/

" Numbers
" Number breakdown:
" \w\@<!                :  Must not be preceded by a word character
" [+-]\?                :  Optional sign
" \(\d*\.\d\+\|\d\+\)   :  One of x.xxx, .xxx or xxx
" \([eE][+-]\?\d\+\)\?  :  Optional exponent
" [pnumkMGT]\?          :  Optional SI suffix
" \w\@!                :  Must not be proceded by a word character
syn match kat2Number contained display /\w\@<![+-]\?\(\d*\.\d\+\|\d\+\)\([eE][+-]\?\d\+\)\?[pnumkMGT]\?\w\@!/

" Variables
syn match kat2Variable contained display /\$\w\+/

" Functions
syn match kat2Function contained display /\w\+(\@=/


"────────────────────┐
" Main control types │
"────────────────────┘

" Only "kat2Element", "kat2Command" or "kat2Gnuplot" can come at the beginning of
" a line
syn match kat2LineStart /^\s*[A-Za-z_]\@=/ nextgroup=kat2Element,kat2Command,kat2Gnuplot

" An element must be followed by a name
syn match kat2Name contained display /[A-Za-z_]\w*/ nextgroup=kat2RestOfLine

" After matching either an element + name or a command, we grab the rest of
" the line and contain all the rest of the rules.
syn match kat2RestOfLine contained /.*$/ contains=ALLBUT,kat2LineStart,kat2Element,kat2Command,kat2Name,kat2Todo,@notesGnuplot

" Highlight gnuplot properly between tags
syn include @notesGnuplot syntax/gnuplot.vim
syn region kat2Gnuplot start="GNUPLOT" end="END" keepend contains=@notesGnuplot


"───────────────┐
" Keyword lists │
"───────────────┘

syn keyword kat2Command contained nextgroup=kat2RestOfLine skipwhite
	\ attr
	\ cav
	\ conf
	\ fadd
	\ fsig
	\ gauss
	\ knm
	\ lambda
	\ map
	\ mask
	\ maxtem
	\ pdtype
	\ phase
	\ retrace
	\ smotion
	\ startnode
	\ tem
	\ tf
	\ tf2
	\ vacuum
	\
	\ const
	\ deriv_h
	\ diff
	\ func
	\ lock
	\ noplot
	\ noxaxis
	\ put
	\ scale
	\ set
	\ trace
	\ var
	\ xaxis
	\ x2axis
	\ yaxis
	\
	\ gnuterm
	\ multi
	\ pause
	\ pyterm

syn keyword kat2Element contained nextgroup=kat2Name skipwhite
	\ bs
	\ bs1
	\ bs2
	\ dbs
	\ isol
	\ l
	\ laser
	\ lens
	\ m
	\ m1
	\ m2
	\ mod
	\ s
	\ sq
	\
	\ ad
	\ beam
	\ bp
	\ cp
	\ guoy
	\ hd
	\ pgaind
	\ qd
	\ qhd
	\ qhdS
	\ qhdN
	\ qnoised
	\ qnoisedS
	\ qnoisedN
	\ qshot
	\ qshotS
	\ qshotN
	\ sd
	\ xd

syn match kat2Element contained nextgroup=kat2Name skipwhite
	\ /\(gr\d\|pd[SN]\?\d*\|q\d*hd\)/

let b:current_syntax = "kat2"

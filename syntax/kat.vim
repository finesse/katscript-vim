" Vim syntax file
" Language: KatScript
" Maintainer: Philip Jones
" Latest Revision: 7 May 2021

if exists("b:current_syntax")
	finish
endif

let b:current_syntax = "kat"

" General philosophy / structure:
"
" The only top-level matches are comments and the special "katLineStart"
" group, so that we can control matches depending on position in the line.
" Everything else is "contained".
"
" "keywords" are used as much as possible rather than matches, as they're much
" more performant and make the code simpler. The main exception is for
" "katKeyword", as some of the keywords are very short and match node /
" parameter names, so we have to do some extra checking that they don't get
" matched where they shouldn't
"
" The somewhat unusual layout of this file is so that the element etc.
" definitions can be appended to the end by a generator script.

" Highlight groups
hi def link katTodo Todo
hi def link katComment Comment
hi def link katBoolean Boolean
hi def link katNumber Number
hi def link katOperator Operator
hi def link katCommand Function
hi def link katFunction Function
hi def link katElement Type
hi def link katKeyword Keyword
hi def link katString String
hi def link katName Identifier


"─────────────┐
" Basic types │
"─────────────┘

" Comments
syn match katComment display /#.*$/ contains=katTodo
syn keyword katTodo contained TODO FIXME

" Strings
syn region katString contained start=+'+ skip=+\\'+ end=+'+
syn region katString contained start=+"+ skip=+\\"+ end=+"+

" Values
" Number breakdown:
" \w\@<!               :  Must not be preceded by a word character
" [+-]\?               :  Optional sign
" \(\d*\.\d\+\|\d\+\)  :  One of x.xxx, .xxx or xxx
" \(...\|...\|...\)\?  :  Optionally one of the following:
"     [pnumkMGT]            :  An SI suffix
"     [eE][+-]\?\d\+[jJ]\?  :  An exponent (possibly with a complex suffix)
"     [jJ]                  :  A complex suffix
" \w\@!                :  Must not be proceded by a word character
syn match katNumber contained display /\w\@<![+-]\?\(\d*\.\d\+\|\d\+\)\([pnumkMGT]\|[eE][+-]\?\d\+[jJ]\?\|[jJ]\)\?\w\@!/
syn keyword katNumber contained inf infj infJ
syn keyword katBoolean contained True true False false

" Operators
syn match katOperator contained display /[+*/&-]/


"────────────────────┐
" Main control types │
"────────────────────┘

" Only "katElement" or "katCommand" can come at the beginning of a normal line
syn match katLineStart /^\s*[A-Za-z_]\@=/ nextgroup=katElement,katCommand

" An element must be followed by a name, and a command must be followed by
" brackets (which may span multiple lines)
syn match katName contained display /[A-Za-z_]\w*/ nextgroup=katRestOfLine
syn region katCommandArgs contained start="(" skip=/(\_.\{-})/ end=")" contains=katRestOfLine keepend
syn region katBrackets contained start="(" end=")" contains=katRestOfLine keepend

" After matching either an element + name or a command, we grab the rest of
" the line and contain all the rest of the rules.
syn match katRestOfLine contained /.*$/ contains=ALLBUT,katLineStart,katElement,katCommand,katName,katTodo,katCommandArgs


"──────────────────────────────┐
" Auto-generated keyword lists │
"──────────────────────────────┘

syn keyword katCommand contained nextgroup=katCommandArgs skipwhite
	\ opt_rf_readout_phase
	\ propagate_beam_astig
	\ frequency_response
	\ sensing_matrix_dc
	\ noise_projection
	\ print_model_attr
	\ noise_analysis
	\ propagate_beam
	\ print_model
	\ beam_trace
	\ run_locks
	\ freqresp
	\ parallel
	\ noxaxis
	\ change
	\ intrix
	\ lambda
	\ series
	\ x2axis
	\ x3axis
	\ debug
	\ modes
	\ print
	\ sweep
	\ xaxis
	\ abcd
	\ fsig
	\ link
	\ plot
	\ tem

syn keyword katElement contained nextgroup=katName skipwhite
	\ quantum_shot_noise_detector_demod_1
	\ quantum_shot_noise_detector_demod_2
	\ quantum_noise_detector_demod_1
	\ quantum_noise_detector_demod_2
	\ quantum_shot_noise_detector
	\ directional_beamsplitter
	\ beam_property_detector
	\ power_detector_demod_1
	\ power_detector_demod_2
	\ quantum_noise_detector
	\ amplitude_detector
	\ degree_of_freedom
	\ power_detector_dc
	\ optical_bandpass
	\ signal_generator
	\ motion_detector
	\ readout_dc_qpd
	\ filter_butter
	\ filter_cheby1
	\ beamsplitter
	\ zpk_actuator
	\ ligo_triple
	\ filter_zpk
	\ readout_dc
	\ readout_rf
	\ amplifier
	\ free_mass
	\ ligo_quad
	\ modulator
	\ actuator
	\ isolator
	\ pendulum
	\ qnoised1
	\ qnoised2
	\ squeezer
	\ variable
	\ ccdline
	\ nothing
	\ qnoised
	\ splitpd
	\ astigd
	\ butter
	\ cavity
	\ cheby1
	\ mirror
	\ qshot1
	\ qshot2
	\ ccdpx
	\ fline
	\ gauss
	\ laser
	\ noise
	\ qshot
	\ space
	\ fcam
	\ gouy
	\ isol
	\ knmd
	\ lens
	\ lock
	\ sgen
	\ amp
	\ cav
	\ ccd
	\ dbs
	\ dof
	\ fpx
	\ mmd
	\ mod
	\ obp
	\ pd1
	\ pd2
	\ var
	\ zpk
	\ ad
	\ bp
	\ bs
	\ cp
	\ pd
	\ sq
	\ xd
	\ l
	\ m
	\ s

syn keyword katFunction contained
	\ geomspace
	\ linspace
	\ logspace
	\ arctan2
	\ deg2rad
	\ degrees
	\ rad2deg
	\ radians
	\ arccos
	\ arcsin
	\ conj
	\ imag
	\ real
	\ sqrt
	\ abs
	\ cos
	\ exp
	\ neg
	\ pos
	\ sin
	\ tan

syn match katKeyword contained display /[A-Za-z0-9._]\@<!\(resolution\|stability\|bandpass\|bandstop\|highpass\|finesse\|lowpass\|modesep\|length\|xsplit\|ysplit\|abcd\|even\|fwhm\|gouy\|loss\|none\|pole\|div\|fsr\|lin\|log\|odd\|off\|tau\|am\|c0\|pi\|pm\|rc\|w0\|zr\|g\|l\|q\|s\|w\|x\|y\|z\)\>/

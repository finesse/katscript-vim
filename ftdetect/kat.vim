au BufNewFile,BufRead *.kat
	\   if getline(1) =~ '#kat2'
	\ |     setlocal filetype=kat2
	\ | else
	\ |     setlocal filetype=kat
	\ | endif
